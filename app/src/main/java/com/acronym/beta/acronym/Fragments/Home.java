package com.acronym.beta.acronym.Fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.HorizontalScrollView;
import com.daprlabs.aaron.swipedeck.SwipeDeck;


import com.acronym.beta.acronym.R;



public class Home extends Fragment {

    public Toolbar toolbar;
    public HorizontalScrollView topSearchesScroll;
    public Home() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        final View view = inflater.inflate(R.layout.fragment_home, container, false);
        topSearchesScroll = (HorizontalScrollView)view.findViewById(R.id.topSearchScroll);
        topSearchesScroll.setHorizontalScrollBarEnabled(false);
        Toolbar toolbar = (Toolbar) view.findViewById(R.id.toolbar);
        ((AppCompatActivity)getActivity()).setSupportActionBar(toolbar);
        ((AppCompatActivity)getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(false);

        return view;
    }

    public static Home newInstance() {
        return new Home();
    }

}
