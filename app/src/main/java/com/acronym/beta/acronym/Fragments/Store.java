package com.acronym.beta.acronym.Fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.acronym.beta.acronym.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class Store extends Fragment {


    public Store() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_store, container, false);
    }

    public static Store newInstance() {
        return new  Store();
    }
}
