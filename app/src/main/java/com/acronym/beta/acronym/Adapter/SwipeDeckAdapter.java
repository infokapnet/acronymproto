package com.acronym.beta.acronym.Adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.acronym.beta.acronym.R;
import com.squareup.picasso.Picasso;

import java.util.List;

import static com.acronym.beta.acronym.R.id.swipeinfooverlay;
import static com.acronym.beta.acronym.R.id.view_offset_helper;

/**
 * Created by Roshan Vadassery on 11/17/2017.
 */

public class SwipeDeckAdapter extends BaseAdapter {

    private List<String> data;
    private Context context;


    public SwipeDeckAdapter(List<String> data, Context context) {
        this.data = data;
        this.context = context;
    }


    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Object getItem(int position) {
        return data.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }


    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        View v = convertView;
        LayoutInflater inflater = LayoutInflater.from(context);;
        if (v == null) {
            // normally use a viewholder
            v = inflater.inflate(R.layout.test_card2, parent, false);
        }
        //((TextView) v.findViewById(R.id.textView2)).setText(data.get(position));
        ImageView imageView = (ImageView) v.findViewById(R.id.offer_image);
        Picasso.with(context).load(R.drawable.bluetop).fit().centerInside().into(imageView);

        v.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.i("Layer type: ", Integer.toString(v.getLayerType()));
                Log.i("Hardware Accel type:", Integer.toString(View.LAYER_TYPE_HARDWARE));
                    /*Intent i = new Intent(v.getContext(), BlankActivity.class);
                    v.getContext().startActivity(i);*/
            }
        });
        final RelativeLayout swipeinfooverlay = (RelativeLayout)v.findViewById(R.id.swipeinfooverlay);
        final ImageView eyeBtn = (ImageView) v.findViewById(R.id.eyeBtn);
        eyeBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (swipeinfooverlay.getAlpha() == 0) {
                    swipeinfooverlay.setAlpha(1);
                    eyeBtn.setImageResource(R.drawable.ic_eye_pressed);
                }
                else {
                    swipeinfooverlay.setAlpha(0);
                    eyeBtn.setImageResource(R.drawable.ic_eye);
                }
            }
        });
        return v;

    }


}
